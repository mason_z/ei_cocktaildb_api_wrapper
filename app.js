var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const v1Router = require('./routes/v1/index');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api/json/v1', v1Router);

module.exports = app;
