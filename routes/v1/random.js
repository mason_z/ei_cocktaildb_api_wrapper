
const express = require('express');
const router = express.Router();

const { randomController } = require('../../controllers');

router.route('/')
    .get(randomController.random);

module.exports = router;