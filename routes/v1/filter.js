
const express = require('express');
const router = express.Router();

const { filterController } = require('../../controllers');

router.route('/')
    .get(filterController.filter);

module.exports = router;