
const express = require('express');
const router = express.Router();

const { cocktailController } = require('../../controllers');

router.route('/:id')
    .get(cocktailController.get)
    .put(cocktailController.put)
    .delete(cocktailController.delete);

module.exports = router;