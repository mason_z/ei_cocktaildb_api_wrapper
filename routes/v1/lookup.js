const express = require('express');
const router = express.Router();

const { lookupController } = require('../../controllers');

router.route('/')
    .get(lookupController.lookup)

module.exports = router;