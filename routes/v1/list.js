const express = require('express');
const router = express.Router();

const { listController } = require('../../controllers');

router.route('/')
    .get(listController.list);


module.exports = router;