const express = require('express');
const router = express.Router();

const { validateApiKey } = require('../../middleware/validation');
const searchRouter = require('./search');
const lookupRouter = require('./lookup');
const randomRouter = require('./random');
const filterRouter = require('./filter');
const listRouter = require('./list');
const cocktailRouter = require('./cocktail');

router.use('/:apiKey/search', validateApiKey, searchRouter);
router.use('/:apiKey/lookup', validateApiKey, lookupRouter);
router.use('/:apiKey/random', validateApiKey, randomRouter);
router.use('/:apiKey/filter', validateApiKey, filterRouter);
router.use('/:apiKey/list', validateApiKey, listRouter);
router.use('/:apiKey/cocktail', validateApiKey, cocktailRouter);

module.exports = router;