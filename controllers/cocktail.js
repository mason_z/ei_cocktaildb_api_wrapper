const config = require('../config');
const axios = require('axios').default;
const db = require('../CustomCocktailDB');

/**
 * Query custom cocktail db first, then look within the real CocktailDB 
 */
const get = (req, res) => {
    const cocktailId = req.params.id;

    let customCocktail = db.find({idDrink: cocktailId});

    if (customCocktail) {
        return res.json(wrapResponse(customCocktail));
    }

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/lookup.php?i=${cocktailId}`
    )
    .then(
        response => res.json(response.data),
        error => {
            throw new Error("Get cocktail query failed: " + error);
        }
    )
}

const put = (req, res) => {
    const cocktailId = req.params.id;

    const cocktail = req.body;
    cocktail.idDrink = cocktailId;

    try {
        let result = db.update({idDrink: cocktailId}, cocktail);
        return res.status(200).json(wrapResponse(result));
    }
    catch(err) {
        const result = db.insert(cocktail);
        return res.status(201).json(wrapResponse(result));
    }
}

const deleteCocktail = (req, res) => {
    const cocktailId = req.params.id;

    let result = db.delete({idDrink: cocktailId});
    let status = result ? 200 : 404;

    return res.status(status).send(result);
}

const wrapResponse = (response) => {
    const res = Array.isArray(response) ? response : [response];

    return {
        drinks: [
            ...res
        ]
    };
}   

module.exports = {
    get,
    put,
    delete: deleteCocktail
}