const config = require('../config');
const axios = require('axios').default;

const list = (req, res) => {
    if (req.query.c) {
        return listCategoryFilters(req, res);
    }

    if (req.query.g) {
        return listGlassFilters(req, res);
    }

    if (req.query.i) {
        return ListIngredientFilters(req, res);
    }

    if (req.query.a) {
        return ListAlcoholicFilters(req, res);
    }

    res.status(400).send('No valid query parameter present on request');
}

const listCategoryFilters = (req, res) => {
    const listValue = req.query.c;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/list.php?c=${listValue}`
    )
    .then(
        response => res.json(response.data),
        error => {
            throw new Error("List of filters query failed: " + error);
        }
    )
}

const listGlassFilters = (req, res) => {
    const listValue = req.query.g;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/list.php?g=${listValue}`
    )
    .then(
        response => res.json(response.data),
        error => {
            throw new Error("List of filters query failed: " + error);
        }
    )
}

const ListIngredientFilters = (req, res) => {
    const listValue = req.query.i;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/list.php?i=${listValue}`
    )
    .then(
        response => res.json(response.data),
        error => {
            throw new Error("List of filters query failed: " + error);
        }
    )
}

const ListAlcoholicFilters = (req, res) => {
    const listValue = req.query.a;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/list.php?a=${listValue}`
    )
    .then(
        response => res.json(response.data),
        error => {
            throw new Error("List of filters query failed: " + error);
        }
    )
}

module.exports = {
    list
}