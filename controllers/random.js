const config = require('../config');
const axios = require('axios').default;

const random = (req, res) => {
    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/random.php`
    )
    .then(
        response => res.json(response.data),
        error => {
            throw new Error("Lookup query failed: " + error);
        }
    )
}

module.exports = {
    random
}