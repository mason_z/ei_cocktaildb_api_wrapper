const config = require('../config');
const axios = require('axios').default;

const search = (req, res) => {
    if (req.query.s) {
        return searchCocktailName(req, res);
    }

    if (req.query.f) {
        return searchCocktailByFirstLetter(req, res);
    }

    if (req.query.i) {
        return searchIngredientName(req, res);
    }

    res.status(400).send('No valid query parameter present on request');
}

const searchCocktailName = (req, res) => {
    const searchValue = req.query.s;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/search.php?s=${searchValue}`
    )
    .then(
        response => res.json(response.data),
        error => {
            throw new Error("Search query failed: " + error);
        }
    )
}

const searchCocktailByFirstLetter = (req, res) => {
    const searchValue = req.query.f;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/search.php?f=${searchValue}`
    ).then(
        response => res.json(response.data),
        error => {
            throw new Error("Search query failed: " + error);
        }
    )
}

const searchIngredientName = (req, res) => {
    const searchValue = req.query.i;
    const apiKey = req.params.apiKey;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/search.php?i=${searchValue}`
    ).then(
        response => res.json(response.data),
        error => {
            throw new Error("Search query failed: " + error);
        }
    )
}



module.exports = {
    search
}