const config = require('../config');
const axios = require('axios').default;

const lookup = (req, res) => {
    if (req.query.i) {
        return lookupCocktailById(req, res);
    }

    if (req.query.iid) {
        return lookupIngredientById(req, res);
    }

    res.status(400).send('No valid query parameter present on request');
}

const lookupCocktailById = (req, res) => {
    const lookupValue = req.query.i;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/lookup.php?i=${lookupValue}`
    )
    .then(
        response => res.json(response.data),
        error => {
            throw new Error("Lookup query failed: " + error);
        }
    )
}

const lookupIngredientById = (req, res) => {
    const lookupValue = req.query.iid;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/lookup.php?iid=${lookupValue}`
    )
    .then(
        response => res.json(response.data),
        error => {
            throw new Error("Lookup query failed: " + error);
        }
    )
}

module.exports = {
    lookup
}