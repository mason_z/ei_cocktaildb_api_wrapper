const config = require('../config');
const axios = require('axios').default;

const filter = (req, res) => {
    if (req.query.i) {
        return filterByIngredient(req, res);
    }

    if (req.query.a) {
        return filterByAlcoholic(req, res);
    }

    if (req.query.c) {
        return filterByCategory(req, res);
    }

    if (req.query.g) {
        return filterByGlass(req, res);
    }

    res.status(400).send('No valid query parameter present on request');
}

const filterByIngredient = (req, res) => {
    const filterValue = req.query.i.split(',');
    const url = (filter) => `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/filter.php?i=${filter}`;

    const filterRequests = filterValue.map(filter => axios.get(url(filter)));

    return Promise.all([...filterRequests]).then(
        response => {
            const combined = {
                drinks: [].concat(...response.map(r => r.data.drinks))
            }

            res.json(combined);
        },
        error => {
            throw new Error("Filter query failed: " + error);
        }
    )
}

const filterByAlcoholic = (req, res) => {
    const filterValue = req.query.a;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/filter.php?a=${filterValue}`
    ).then(
        response => res.json(response.data),
        error => {
            throw new Error("Filter query failed: " + error);
        }
    )
}

const filterByCategory = (req, res) => {
    const filterValue = req.query.c.split(',');
    const url = (filter) => `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/filter.php?c=${filter}`;

    const filterRequests = filterValue.map(filter => axios.get(url(filter)));

    return Promise.all([...filterRequests]).then(
        response => {
            const combined = {
                drinks: [].concat(...response.map(r => r.data.drinks))
            }

            res.json(combined);
        },
        error => {
            throw new Error("Filter query failed: " + error);
        }
    )
}

const filterByGlass = (req, res) => {
    const filterValue = req.query.g;

    return axios.get(
        `${config.COCKTAILDB_V1_API_URL}/${req.apiKey}/filter.php?g=${filterValue}`
    ).then(
        response => res.json(response.data),
        error => {
            throw new Error("Filter query failed: " + error);
        }
    )
}

module.exports = {
    filter
}