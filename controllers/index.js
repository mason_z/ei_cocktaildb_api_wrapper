module.exports.searchController = require('./search');
module.exports.lookupController = require('./lookup');
module.exports.randomController = require('./random');
module.exports.filterController = require('./filter');
module.exports.listController = require('./list');
module.exports.cocktailController = require('./cocktail');
