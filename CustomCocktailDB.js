const lodash = require('lodash');

let store = [];

const find = (filter) => {
    return lodash.find(store, filter);
}

const insert = (cocktail) => {
    let cocktailIndex = lodash.findIndex(store, {idDrink: cocktail.id});

    if (cocktailIndex !== -1) {
        throw new Error('Cocktail already exists in db');
    }

    store.push(cocktail);
    
    return cocktail;
}

const update = (filter, cocktail) => {
    let cocktailIndex = lodash.findIndex(store, filter);

    if (cocktailIndex === -1) {
        throw new Error('Cocktail does not exist in db');
    }

    let updateCocktail = store[cocktailIndex];

    updateCocktail = Object.assign(updateCocktail, cocktail);

    store.splice(cocktailIndex, 1, updateCocktail);

    return updateCocktail;
}

const deleteItem = (filter) => {
    let cocktailIndex = lodash.findIndex(store, filter);
    
    if (cocktailIndex !== -1) {
        store.splice(cocktailIndex, 1);
    } else {
        return false;
    }

    return true;
}


module.exports = {
    find,
    insert,
    update,
    delete: deleteItem
}