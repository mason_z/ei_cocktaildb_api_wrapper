
const validApiKeys = [
    1
];

function validateApiKey (req, res, next) {
    let apiKey = req.params.apiKey;

    if (!apiKey) {
        res.status(401).send('Missing API key');
    }

    if (!validApiKeys.includes(+apiKey)) {
        res.status(401).send('Bad API key');
    }

    req.apiKey = apiKey;

    next();
}

module.exports.validateApiKey = validateApiKey;