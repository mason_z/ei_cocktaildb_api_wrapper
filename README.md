# Coding Exercise for The Expert Institue

To run project:

    git clone https://gitlab.com/mason_z/ei_cocktaildb_api_wrapper.git
    npm install
    npm start

This API is called the same way as TheCocktailDB, except without the `.php`.

Examples: 

> http://localhost:3000/api/json/v1/1/search?s=margarita

> http://localhost:3000/api/json/v1/1/lookup?i=11007

> http://localhost:3000/api/json/v1/1/search?i=vodka

> http://localhost:3000/api/json/v1/1/random  

  

------------------  


New functionality

Ingredients and categories can both have multiple filters  


> http://localhost:3000/api/json/v1/1/filter?i=Gin,Scotch

> http://localhost:3000/api/json/v1/1/filter?c=Ordinary_Drink,Cocktail,Cocoa  



Users can perform CRUD on custom cocktails   

> GET, PUT, DELETE http://localhost:3000/api/json/v1/1/cocktail/50000
>> Add cocktail object as request body for PUT  


*Custom cocktails are not persistent*
